## Figure mouse weights

# see corresponding lecture here:
# http://genomicsclass.github.io/book/pages/getting_started.html


dat <- read.csv("femaleMiceWeights.csv")

bodyweight_plot <-
ggplot(dat, aes(x=Diet, y=Bodyweight, color=Diet))+
  geom_boxplot()+
  geom_jitter(width=0.1)+
  theme_classic()+
  scale_y_continuous(limits=c(0,35))

ggsave("two-sample-mouseweights.png", plot=bodyweight_plot,
       width=5,height=3)

### t test
t.test(Bodyweight ~ Diet, data = dat)
