# CSDA tutorial - Hypothesis Testing

## Learning objectives:  
 
- understand the common principles behind statistical tests  
- learn to spot common pitfalls  
- perform simple tests in R  

## How to run the tutorial

**Online:**
The tutorial is available [here](https://shiny-portal.embl.de/shinyapps/app/03_csda_tutorial_3).

**Manually:**  

If you'd like to run the tutorial locally:

- download / clone this repository
- Open RStudio  
- Install the `learnr` package  
- open the file `csda-tutorial-testing.Rmd` in Rstudio  
- Click *Run Document*  



